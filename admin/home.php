<?php
  $sql1 = 'SELECT COUNT(*) total FROM usuarios WHERE rol_id = 2';
  $sql2 = 'SELECT SUM(cantidad) total FROM acciones_cliente';
  $sql3 = 'SELECT SUM(total_orden) as total_ventas FROM acciones_cliente';
  $connection = new mysqli('localhost', 'root', 'Logaritmos506', 'primer_proyecto_web1');

  $result1 = mysqli_query($connection,$sql1);
  $fila1 = mysqli_fetch_assoc($result1);

  $result2 = mysqli_query($connection,$sql2);
  $fila2 = mysqli_fetch_assoc($result2);

  $result3 = mysqli_query($connection,$sql3);
  $fila3 = mysqli_fetch_assoc($result3);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title>
    <link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
</head>
<style>
#g-table tbody tr > td{
    border: 1px solid rgb(220,220,220);
    height: 30px;
    padding-left: 3px;
}
#g-table{
    padding-left: 40px;
    margin-top: 20px;

}
nav > ul {
  display: flex;
  flex-direction: column;
  align-items: center;
}
</style>
<body style="background-color:gray">  
        <?php
            if(isset($errorLogin)){
                echo $errorLogin;
            }
        ?>
    <div id="menu">
        <ul>
            <li>Home - Administrador</li>
            <li class="cerrar-sesion"><a href="includes/logout.php">Cerrar sesión</a></li>
        </ul>
    </div>
    <section>
        <h1 style="color:white;">Bienvenido <?php echo $user->getNombre();  ?></h1>  
    </section>
    <table align="center" class="table table-light"  id="g-table" style="text-align:center;">
      <tr>
        <th>Clientes registrados </th>
        <th>Productos vendidos </th>
        <th>Total de ventas </th>
      </tr>
      <tbody>
        <?php  
              echo "<tr><td>".$fila1['total']."</td><td>".$fila2['total']."</td><td>".$fila3['total_ventas']."</td></tr>";   
        ?>
      </tbody>
    </table>
    <div style="text-align: center;">
      <a href='index_admin.php?page=admin/admin_categorias.php'><input type="button" value="Administrar Categorías"></a>
      <a href='index_admin.php?page=admin/admin_productos.php'><input type="button" value="Administrar Productos"></a>
    </div>
</body>
</html>