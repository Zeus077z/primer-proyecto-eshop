<?php
$id = $_GET['id'];
$sql1 = "SELECT * FROM productos WHERE sku=$id";
$sql2 = "SELECT * FROM categorias";
$connection = new mysqli('localhost', 'root', 'Logaritmos506', 'primer_proyecto_web1');

$result1 = $connection->query($sql1);
$productos = $result1->fetch_all();

foreach($productos as $producto){   
    $nombre =$producto[1];
    $descripcion =$producto[2];
    $imagen = $producto[3];
    $categ =$producto[4];
    $stock =$producto[5];
    $precio =$producto[6];
}

$result2 = $connection->query($sql2);
$categorias = $result2->fetch_all();

if(isset($_POST['estado']))
    {
        $categ =$_POST['estado'];
        $nombre =$_POST['txtNombre'];
        $descripcion =$_POST['txtDescripcion'];
        $imagen = $_FILES['txtImagen']['name'];

        $fecha = new DateTime();
        $nombreArchivo=($imagen!="")?$fecha->getTimestamp()."_".$_FILES["txtImagen"]["name"]:"producto.png";
        $tmpImagen=$_FILES["txtImagen"]["tmp_name"];
        if($tmpImagen!=""){
            move_uploaded_file($tmpImagen,"images/".$nombreArchivo);
        }

        $stock =$_POST['txtStock'];
        $precio =$_POST['txtPrecio'];
        $sql = "UPDATE productos SET nombre='$nombre', descripcion='$descripcion', imagen='$nombreArchivo', categoria='$categ', stock='$stock', precio='$precio' WHERE sku=$id";
        $connection = mysqli_connect('localhost', 'root', 'Logaritmos506', 'primer_proyecto_web1');
        mysqli_query($connection, $sql);
        mysqli_close($connection);
        header('Location: index_admin.php?page=admin/admin_productos.php');
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <title>Document</title>
</head>
<body style="background-color:gray">
    <div class="container">
        <form action="" method="POST" enctype="multipart/form-data">
            <label for="">Nombre:</label>
            <input type="text" name="txtNombre" placeholder="" id="txt1" require="" value=<?php echo $nombre ?>>
            <br>
            <label for="">Descripción:</label>
            <input type="text" name="txtDescripcion" placeholder="" id="txt2" require="" value=<?php echo $descripcion ?>>
            <br>
            <label for="">Imagen:</label>
            <input type="file" accept="image/*" name="txtImagen" placeholder="" id="txt3" require="" value=<?php echo $imagen ?>>
            <br>
            <label for="">Categoría:</label>
            <select name="estado">
                <?php
                    foreach($categorias as $categoria) {
                    echo "<option value=".$categoria[0].">".$categoria[1]."</option>";
                    }
                ?>
            </select>
            <br>
            <label for="">Stock:</label>
            <input type="text" name="txtStock" placeholder="" id="txt6" require="" value=<?php echo $stock ?>>
            <br>
            <label for="">Precio:</label>
            <input type="text" name="txtPrecio" placeholder="" id="txt7" require="" value=<?php echo $precio ?>>
            <br>
            <input type="submit" value="Editar">
            <a href="index_admin.php?page=admin/admin_productos.php"><input type="button" value="Cancelar"></a> 
        </form>
    </div>
</body>
</html>