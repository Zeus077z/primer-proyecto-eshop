<?php
  $sql = 'SELECT productos.sku, productos.nombre, productos.descripcion, productos.imagen, categorias.categoria, productos.stock, productos.precio
  FROM productos, categorias 
 WHERE productos.categoria=categorias.id';
  $connection = new mysqli('localhost', 'root', 'Logaritmos506', 'primer_proyecto_web1');
  $result = $connection->query($sql);
  $productos = $result->fetch_all();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title>
    <link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
</head>
<style>
#g-table tbody tr > td{
    border: 1px solid rgb(220,220,220);
    height: 30px;
    padding-left: 3px;
}
#g-table{
    padding-left: 40px;
    margin-top: 20px;

}
nav > ul {
  display: flex;
  flex-direction: column;
  align-items: center;
}

</style>
<body style="background-color:gray">
    <div id="menu">
        <ul>
            <li>Home - Administrador</li>
            <li class="cerrar-sesion"><a href="includes/logout.php">Cerrar sesión</a></li>
        </ul>
    </div>
    <section>
        <h1 style="color:white;">Bienvenido <?php echo $user->getNombre();  ?></h1>  
    </section>
    <table align="center" class="table table-light"  id="g-table">
      <tr>
        <th>SKU</th>
        <th>Nombre</th>
        <th>Descripción</th>
        <th>Imagen</th>
        <th>Categoria</th>
        <th>Stock</th>
        <th>Precio</th>
      </tr>
      <tbody>
        <?php
          $page = 'admin/admin_productos.php';  
            foreach ($productos as $producto) {
                if($producto[3] == null){
                  $imagen="producto.png";
                  $producto[3] = $imagen;
                }
                echo "<tr><td>".$producto[0]."</td><td>".$producto[1]."</td><td>".$producto[2]."</td><td><img src='images/$producto[3].' width='100px' class='img-thumbnail'></td><td>".$producto[4]."</td><td>".$producto[5]."</td><td>".$producto[6]."</td><td><a href='index_admin.php?page=admin/edit_producto.php&id=".$producto[0]."'><input type='button' value='Editar 📝'></a><td><a href='delete.php?id=".$producto[0]."&table=productos&field=sku&page=index_admin.php?page=".$page."'><input type='button' value='Eliminar 🗑'></td></a></tr>";  
            }             
        ?>
      </tbody>
    </table>
    <div style="text-align: center;">
      <a href="index.php"><input type="button" value="Home - Admin"></a>
      <a href="index_admin.php?page=admin/admin_nuevo_producto.php"><input type="button" value="Nuevo producto"></a>
    </div>
</body>
</html>