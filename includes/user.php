<?php
include 'db.php';

class User extends DB{
    private $nombre;
    private $username;

    /**
     * Realiza una consulta para saber si el usuario existe en la base de datos.
     */
    public function userExists($user, $pass){
        $md5pass = md5($pass);
        $query = $this->connect()->prepare('SELECT * FROM usuarios WHERE name = :user AND password = :pass');
        $query->execute(['user' => $user, 'pass' => $md5pass]);

        if($query->rowCount()){
            return true;
        }else{
            return false;
        }
    }
    /**
     * Envia el usuario encontrado para que pueda ser usados sus 3 atributos mas importantes.
     */
    public function setUser($user){
        $query = $this->connect()->prepare('SELECT * FROM usuarios WHERE name = :user');
        $query->execute(['user' => $user]);
        
        foreach ($query as $currentUser) {
            $this->id = $currentUser['id'];
            $this->nombre = $currentUser['name'];
            $this->rol_id = $currentUser['rol_id'];
        }
    }
    public function getId(){
        return $this->id;
    }
    public function getNombre(){
        return $this->nombre;
    }
    public function getRol(){
        return $this->rol_id;
    }
}

?>