<?php
    $id = $user->getId();
    $connection = mysqli_connect('localhost', 'root', 'Logaritmos506', 'primer_proyecto_web1');
    $sql1 = 'SELECT * FROM categorias';
    $result1 = $connection->query($sql1);
    $categorias = $result1->fetch_all();

    if(isset($_GET['id_p'])){
        $id_p = $_GET['id_p'];
        $id_cat = $_GET['id_cat'];     
        $sql2 = "SELECT id, cantidad FROM carrito_compras where id_producto = $id_p and id_usuario = $id";
        $result2 = $connection->query($sql2);
        $carrito = $result2->fetch_all();
        $existencia = 0;
        $cantidad = 0;
        foreach($carrito as $car){
            $existencia = $car[0];
            $cantidad = $car[1];
        } 
        if($existencia != 0){
            $cantidad += 1;
            $sql = "UPDATE carrito_compras SET cantidad=$cantidad WHERE id=$existencia";
        }else{
            $sql = "INSERT INTO carrito_compras (id_producto, id_usuario) VALUES ('$id_p','$id')";
        }    
        mysqli_query($connection, $sql);
        mysqli_close($connection);
        header("Location: index_cliente.php?page=cliente/home.php&id_cat=$id_cat");
    } 

    if(isset($_GET['id_cat'])){
        $id_cat= $_GET['id_cat'];
        $sql2 = "SELECT productos.sku, productos.nombre, productos.descripcion, productos.imagen, categorias.categoria, productos.stock, productos.precio
        FROM productos, categorias 
        WHERE productos.categoria = categorias.id and categorias.id = '$id_cat'";
        $result2 = $connection->query($sql2);
        $productos = $result2->fetch_all();
    }

           
    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
</head>
<style>
ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #333;
}

li {
  float: left;
  border-right:1px solid #bbb;
}

li:last-child {
  border-right: none;
}

li a {
  display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li a:hover:not(.active) {
  background-color: #111;
}

.active {
  background-color: purple;
}
#g-table tbody tr > td{
    height: 30px;
    padding-left: 3px;
}
#g-table{
    padding-left: 40px;
    margin-top: 20px;
}
nav > ul {
  display: flex;
  flex-direction: column;
  align-items: center;
}

</style>
<body style="background-color:gray">  
        <?php
            if(isset($errorLogin)){
                echo $errorLogin;
            }
        ?>
    <div id="menu">
        <ul>
            <?php
                foreach ($categorias as $categoria) {
                    if(isset($_GET['id_cat'])){
                        $id_cat = $_GET['id_cat'];
                        if($categoria[0]==$id_cat){
                            echo "<li style='float:left' class='active'><a href='index_cliente.php?page=cliente/home.php&id_cat=$categoria[0]'>$categoria[1]</a></li>";  
                        }else{
                            echo "<li style='float:left'><a href='index_cliente.php?page=cliente/home.php&id_cat=$categoria[0]'>$categoria[1]</a></li>";
                        }
                    }else{
                        echo "<li style='float:left'><a href='index_cliente.php?page=cliente/home.php&id_cat=$categoria[0]'>$categoria[1]</a></li>";
                    }
                      
                }        
            ?>   
                   
            <li style="float:right"><a href="includes/logout.php">Cerrar sesión</a></li>
            <?php
                $sql2 = "SELECT SUM(cantidad) as total FROM carrito_compras WHERE id_usuario = $id";
                $result2 = mysqli_query($connection,$sql2);
                $fila1 = mysqli_fetch_assoc($result2);
                $num = $fila1['total'];
                if($num!=0){
                    echo "<li style='float:right' class='active'><a href='index_cliente.php?page=cliente/carrito.php'>🛒 ($num)</a></li>";
                }else{
                    echo "<li style='float:right' class='active'><a href='index_cliente.php?page=cliente/carrito.php'>🛒</a></li>";
                }
            ?> 
             <li style="float:right"><a href="index_cliente.php?page=cliente/ver_compras.php">Ver compras realizadas</a></li> 
        </ul>
    </div>
    <section>
        <?php
            $nombre = $user->getNombre();
            if(isset($_GET['id_cat'])){
                echo "";
            }else{
                echo "<h1 style='color:white;'>Hola $nombre! Selecciona una categoría de productos!</h1>";
                $sql6 = "SELECT SUM(cantidad) as total FROM acciones_cliente where id_usuario = $id";
                $result6 = mysqli_query($connection,$sql6);
                $fila6 = mysqli_fetch_assoc($result6);
                $cantidad_productos = $fila6['total'];

                $sql7 = "SELECT SUM(total_orden) as total FROM acciones_cliente where id_usuario = $id";
                $result7 = mysqli_query($connection,$sql7);
                $fila7 = mysqli_fetch_assoc($result7);
                $total_orden = $fila7['total'];
                if($cantidad_productos == null){
                    $cantidad_productos = 0;
                }
                if($total_orden == null){
                    $total_orden = 0;
                }
                echo "<tr><td><h3 style='color:purple';>Total productos adquiridos: <strong style='color:white';>$cantidad_productos</strong></h3><h3 style='color:purple';>Monto total de compras realizadas: <strong style='color:white';>₡$total_orden</strong></h3></td></tr>";
            }      
        ?>     
    </section>
    <table align="center" class="table table-light"  id="g-table">
    <form action="" method="POST" ectype="multipart/form-data">
      <tbody>
        <?php
            if(isset($_GET['id_cat'])){
                $id_cat = $_GET['id_cat'];
                foreach ($productos as $producto) {
                    if($producto[3] == null){
                      $imagen="producto.png";
                      $producto[3] = $imagen;
                    }
                    $sql2 = "SELECT cantidad FROM carrito_compras where id_producto = $producto[0] and id_usuario = $id";
                    $result5 = mysqli_query($connection,$sql2);
                    $fila5 = mysqli_fetch_assoc($result5);
                    $cantidad = isset($fila5['cantidad']) ? $fila5['cantidad'] : 0;
                    $stock = $producto[5];        
                    if($cantidad >= $stock){   
                        echo "<tr><td><img src='images/$producto[3].' width='100px' class='img-thumbnail'></td><td><h5>$producto[1] $producto[2]</h5><h5>₡$producto[6]</h5></td><td><h5>".$producto[5]." en stock.</h5></td><td><a href='index_cliente.php?page=cliente/vista.php&id_pr=".$producto[0]."&id_ca=".$id_cat."'><input type='button' value='Ver 👁'></a><a href='index_cliente.php?page=cliente/home.php&id_cat=".$id_cat."&id_p=".$producto[0]."'><input type='button' value='Añadir 🛒' disabled='true'></a></td></tr>";
                    }else{
                        echo "<tr><td><img src='images/$producto[3].' width='100px' class='img-thumbnail'></td><td><h5>$producto[1] $producto[2]</h5><h5>₡$producto[6]</h5></td><td><h5>".$producto[5]." en stock.</h5></td><td><a href='index_cliente.php?page=cliente/vista.php&id_pr=".$producto[0]."&id_ca=".$id_cat."'><input type='button' value='Ver 👁'></a><a href='index_cliente.php?page=cliente/home.php&id_cat=".$id_cat."&id_p=".$producto[0]."'><input type='button' value='Añadir 🛒'></a></td></tr>";
                    }             
                }
            }                               
        ?>
      </tbody>
      </form>
    </table>
</body>
</html>