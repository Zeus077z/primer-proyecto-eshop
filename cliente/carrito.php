<?php

$connection = mysqli_connect('localhost', 'root', 'Logaritmos506', 'primer_proyecto_web1');

if(isset($_GET["table"])){
  $tblname=$_GET["table"]; 
  $field_id = $_GET["field"];
  $id= $_GET["id"];
  $sql = "DELETE FROM ".$tblname." WHERE ".$field_id."=".$id."";
  mysqli_query($connection,$sql);
  header('Location: index_cliente.php?page=cliente/carrito.php');
}

$id = $user->getId();

$sql = "SELECT productos.sku, productos.nombre, productos.descripcion, productos.imagen, categorias.categoria, productos.stock, productos.precio
FROM productos, categorias, carrito_compras 
WHERE productos.categoria = categorias.id and productos.sku = carrito_compras.id_producto and carrito_compras.id_usuario = $id";

$result = $connection->query($sql);
$productos = $result->fetch_all();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
</head>
<style>
ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #333;
}

li {
  float: left;
  border-right:1px solid #bbb;
}

li:last-child {
  border-right: none;
}

li a {
  display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li a:hover:not(.active) {
  background-color: #111;
}

li input {
  display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  background-color: purple;
}

li input:hover:not(.active) {
  background-color: #111;
}

.active {
  background-color: purple;
}
#g-table tbody tr > td{
    border: 1px solid rgb(220,220,220);
    height: 30px;
    padding-left: 3px;
}
#g-table{
    padding-left: 40px;
    margin-top: 20px;
}
nav > ul {
  display: flex;
  flex-direction: column;
  align-items: center;
}
</style>
<body style="background-color:gray">
<form action="" method="POST" ectype="multipart/form-data">
    <div id="menu">
        <ul>          
            <li style="float:left"><input type="submit" value="Actualizar carrito" name="actualizar"></li>
            <li style="float:right"><a href="index_cliente.php?page=cliente/ver_compras.php">Ver compras</a></li>
            <li style="float:right"><a href="includes/logout.php">Cerrar sesión</a></li>
            <li style="float:right"><a href="index_cliente.php?page=cliente/home.php">Volver</a></li>      
        </ul>
    </div>
    <table align="center" class="table table-light"  id="g-table">
      <tbody>     
          <?php           
              $monto_total = 0;
              foreach ($productos as $producto) {
                  $sql2 = "SELECT id, cantidad FROM carrito_compras where id_producto = $producto[0] and id_usuario = $id ";
                  $result2 = mysqli_query($connection,$sql2);
                  $fila2 = mysqli_fetch_assoc($result2);

                  $sql3 = "SELECT SUM(precio) * carrito_compras.cantidad as total_ventas FROM productos, carrito_compras 
                  WHERE productos.sku = carrito_compras.id_producto and productos.sku = $producto[0] and carrito_compras.id_usuario = $id";
                  $result3 = mysqli_query($connection,$sql3);
                  $fila3 = mysqli_fetch_assoc($result3);

                  $sql4 = "SELECT id FROM carrito_compras where id_producto = $producto[0] and id_usuario = $id ";
                  $result4 = mysqli_query($connection,$sql4);
                  $fila4 = mysqli_fetch_assoc($result4);

                  $precio_x_producto = $fila3['total_ventas'];                 
                  $monto_total += $precio_x_producto;
                  $cantidad = $fila2['cantidad'];
                  $id_producto_carrito = $fila4['id'];

                  if($producto[3] == null){
                    $imagen="producto.png";
                    $producto[3] = $imagen;
                  }
                  $page = 'cliente/carrito.php';
                  echo "<tr><td><img src='images/$producto[3].' width='100px' class='img-thumbnail'></td><td><h5>$producto[1] $producto[2]</h5><h5>₡$producto[6]</h5></td><td>Total productos: <input type='number' name='$producto[0]' min='1' max='$producto[5]' value=$cantidad></td><td><h5>Precio total: ₡$precio_x_producto</h5></td><td><a href='index_cliente.php?id=".$id_producto_carrito."&table=carrito_compras&field=id&page=cliente/carrito.php'><input type='button' value='Eliminar 🗑'></td></a></tr>";  
              }
              if($monto_total <= 0){
                echo "<h4>No hay productos en el carrito.</h4>";
                echo "<table align='center' class='table table-light'  id='g-table'>
                      <tbody>
                          <tr><td><strong>Monto total: ₡$monto_total</strong></td><td><a href='index_cliente.php?page=cliente/orden.php'><input type='button' value='Realizar compra' disabled='true'></td></tr>
                      </tbody>
                    </table>";
              }else{
                echo "<table align='center' class='table table-light'  id='g-table'>
                <tbody>
                    <tr><td><strong>Monto total: ₡$monto_total</strong></td><td><a href='index_cliente.php?page=cliente/orden.php'><input type='button' value='Realizar compra' ></td></tr>
                </tbody>
              </table>";
              }            
              if(isset($_POST['actualizar'])){
                if($monto_total > 0){
                  foreach($productos as $producto){
                    $s = $_POST[$producto[0]];
                    $connection = mysqli_connect('localhost', 'root', 'Logaritmos506', 'primer_proyecto_web1');
                    $sql5 = "UPDATE carrito_compras SET cantidad=$s WHERE id_usuario=$id and id_producto=$producto[0]";
                    mysqli_query($connection, $sql5);
                    mysqli_close($connection);
                    header('Location: index_cliente.php?page=cliente/carrito.php');
                  } 
                }
                echo "No hay productos en el carrito que actualizar.";            
              }
                                  
          ?>
        </form>   
      </tbody>
    </table>
</body>
</html>